import {FastifyInstance} from "fastify";

declare global {
    namespace NodeJS {
        interface Global {
            server: FastifyInstance
        }
    }
}

import {expect} from 'chai';
import {it} from 'mocha';
import {Response} from "light-my-request";


import {getFirestore} from "../../src/plugins/StoragePlugin";
import {Key} from "../../src/entities/Key";


describe('API token tests', () => {

    let fastify = global.server;
    it("Check availability of api", async () => {
        let response: Response = await fastify.inject({
            method: "GET",
            url: '/token'
        });
        expect(response.statusCode).to.be.a("number");
        expect(response.statusCode).to.be.equal(200);
        let payload = response.json();
        expect(payload).to.be.an("object");
        expect(payload).to.have.property("publicKey");
        expect(payload.publicKey).to.be.a("string");

        let db = getFirestore();
        let keysStorage = await db.collection('tokens').doc("general").get();
        expect(keysStorage.exists).to.be.true;

        let data = keysStorage.data() as Key;
        expect(data).not.to.be.undefined;
        expect(data).to.have.property("publicKey");

        expect(data.publicKey).to.be.equal(payload.publicKey);

    });

});