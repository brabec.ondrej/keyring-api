import {FastifyInstance} from "fastify";
import {ntru} from "ntru";

import {Response} from "light-my-request";
import {EncryptionPlugin} from "../../src/plugins/EncryptionPlugin";
import {Base64ArrayBufferConvertor} from "../../src/utils/Base64ArrayBufferConvertor";
import {expect} from "chai";
import {LoginResponseInterface} from "../../src/types/LoginResponseSchema";

declare global {
    namespace NodeJS {
        interface Global {
            serverKey: string
            server: FastifyInstance
            token: string
        }
    }
}


describe('API login tests', () => {

    it("Should succesfully Login", async () => {
        let key: { privateKey: Uint8Array; publicKey: Uint8Array; };
        key = await ntru.keyPair();
        let data = {
            publicKey: Base64ArrayBufferConvertor.encode(key.publicKey),
            email: "random@test.com",
            password: "123456789abcDEF"
        };

        let body = await EncryptionPlugin.encrypt(data, global.serverKey);

        let login: Response = await global.server.inject({
            method: "POST",
            url: '/login',
            payload: body
        });

        let encryptedBody = JSON.parse(login.payload);

        expect(login.statusCode).to.be.equal(200);
        let loginBody = await EncryptionPlugin.decrypt(encryptedBody, Base64ArrayBufferConvertor.encode(key.privateKey)) as LoginResponseInterface;
        expect(loginBody).to.have.any.keys("token");
        expect(loginBody.token).to.be.an("string");
    });

    it("Should unsuccessfully Login", async () => {
        let key: { privateKey: Uint8Array; publicKey: Uint8Array; };
        key = await ntru.keyPair();
        let data = {
            publicKey: Base64ArrayBufferConvertor.encode(key.publicKey),
            email: "random@test.com",
            password: "123456789DEF"
        };
        let body = await EncryptionPlugin.encrypt(data, global.serverKey);

        let login: Response = await global.server.inject({
            method: "POST",
            url: '/login',
            payload: body
        });

        expect(login.statusCode).to.be.equal(403);

        let encryptedBody = JSON.parse(login.payload);

        let loginBody = await EncryptionPlugin.decrypt(encryptedBody, Base64ArrayBufferConvertor.encode(key.privateKey)) as LoginResponseInterface;
        expect(loginBody).to.have.any.keys("error", "message");
        expect(loginBody.message).to.be.equal("Wrong password!");
    });


});