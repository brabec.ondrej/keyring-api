import {FastifyInstance} from "fastify";
import {Response} from "light-my-request";
import {expect} from "chai";
import {E2EEncryptionUtil} from "../../src/plugins/E2EEncryptionPlugin";
import {EncryptionPlugin} from "../../src/plugins/EncryptionPlugin";
import {Group} from "../../src/entities/Group";
import {it} from "mocha";
import {GroupsResponseInterface} from "../../src/types/GroupsResponseSchema";
import {GroupDetailResponseInterface} from "../../src/types/GroupDetailResponseSchema";
import {Base64ArrayBufferConvertor} from "../../src/utils/Base64ArrayBufferConvertor";
import {StatusResponseInterface} from "../../src/types/StatusResponseSchema";
import {EncryptionUtil} from "../../src/utils/EncryptionUtil";

declare global {
    namespace NodeJS {
        interface Global {
            serverKey: string
            localKey: string
            server: FastifyInstance
            token: string
        }
    }
}

describe('API group tests', () => {
    it("Should Return list of groups", async () => {
        let groups: Response = await global.server.inject({
            method: "GET",
            url: '/groups',
            headers: {
                authorization: "Bearer "+ global.token
            }
        });


        expect(groups.statusCode).to.be.equal(200);

        let decryptPayload = await EncryptionPlugin.decrypt(groups.json(), global.localKey) as GroupsResponseInterface;

        expect(decryptPayload).to.be.an("array");
        expect(decryptPayload.length).to.have.be.least(1);

        expect(decryptPayload[0]).to.have.keys("name","publicKey","id");
        expect(decryptPayload[0].name).to.be.equal("Personal");
    });
    it("Should not be able without token",async ()=> {
        let groups: Response = await global.server.inject({
            method: "GET",
            url: '/groups',
        });

        expect(groups.statusCode).to.be.equal(400);

    });
    it("Should not be able with wrong token",async ()=> {
        let groups: Response = await global.server.inject({
            method: "GET",
            url: '/groups',
            headers: {
                authorization: "Bearer asdfasdfa74asd355f5as5dfasdfsdfasdf"
            }
        });

        expect(groups.statusCode).to.be.equal(403)
    });

    it("Should not be able to get one group",async ()=> {
        let groups: Response = await global.server.inject({
            method: "GET",
            url: '/groups',
            headers: {
                authorization: "Bearer "+ global.token
            }
        });
        let decryptPayload = await EncryptionPlugin.decrypt(groups.json(), global.localKey) as GroupsResponseInterface;

        let groupData = decryptPayload[0];

        let singleGroup: Response = await global.server.inject({
            method: "GET",
            url: '/group/'+groupData.id,
            headers: {
                authorization: "Bearer "+ global.token
            }
        });
        expect(singleGroup.statusCode).to.be.equal(200)
        let decryptSinglePayload = await EncryptionPlugin.decrypt(singleGroup.json(), global.localKey) as GroupDetailResponseInterface;

        expect(decryptSinglePayload).to.be.an("object");
        expect(decryptSinglePayload).to.have.keys("name","publicKey","resources");
        expect(decryptSinglePayload.resources).to.be.an("array");
    });

    it("Should be able to create Group",async ()=> {

        let keys = await EncryptionUtil.generateKeys();

        let data = {
            name: "New group",
            publicKey: Base64ArrayBufferConvertor.encode(keys.publicKey)
        };


        let body = await EncryptionPlugin.encrypt(data, global.serverKey);

        let createGroup: Response = await global.server.inject({
            method: "POST",
            url: '/group',
            headers: {
                authorization: "Bearer "+ global.token
            },
            payload: body
        });


        let decryptPayload = await EncryptionPlugin.decrypt(createGroup.json(), Base64ArrayBufferConvertor.encode(keys.privateKey)) as StatusResponseInterface;

        expect(createGroup.statusCode).to.be.equal(201);
        expect(decryptPayload).to.have.keys("status","message");

        expect(decryptPayload.status).to.be.equal("OK");

        let groups: Response = await global.server.inject({
            method: "GET",
            url: '/groups',
            headers: {
                authorization: "Bearer "+ global.token
            }
        });

        expect(groups.statusCode).to.be.equal(200);

        let groupList = await EncryptionPlugin.decrypt(groups.json(), global.localKey) as GroupsResponseInterface;

        expect(groupList).to.be.an("array");

        let result = null;
        groupList.forEach((i) => {
            if(i.name === data.name) {
                result = data.name;
            }
        })

        expect(result).to.be.a("string");
        expect(result).to.be.equal(data.name);

    });

    it("Should remove the group",async () => {
        let groups: Response = await global.server.inject({
            method: "GET",
            url: '/groups',
            headers: {
                authorization: "Bearer "+ global.token
            }
        });
        let decryptPayload = await EncryptionPlugin.decrypt(groups.json(), global.localKey) as GroupsResponseInterface;

        let groupData = decryptPayload[1];
        let removeGroup: Response = await global.server.inject({
            method: "DELETE",
            url: '/group/'+groupData.id,
            headers: {
                authorization: "Bearer "+ global.token
            }
        });
        let decryptRemovePayload = await EncryptionPlugin.decrypt(removeGroup.json(), global.localKey) as StatusResponseInterface;


        expect(removeGroup.statusCode).to.be.equal(204);

        expect(decryptRemovePayload).to.have.keys("status","message");

        expect(decryptRemovePayload.status).to.be.equal("OK");

        let groupsAgain: Response = await global.server.inject({
            method: "GET",
            url: '/groups',
            headers: {
                authorization: "Bearer "+ global.token
            }
        });

        expect(groupsAgain.statusCode).to.be.equal(200);

        let decryptedAgainPayload = await EncryptionPlugin.decrypt(groupsAgain.json(), global.localKey) as GroupsResponseInterface;
        expect(decryptPayload).not.to.be.equal(decryptedAgainPayload);

    });
});