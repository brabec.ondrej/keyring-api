declare global {
    namespace NodeJS {
        interface Global {
            serverKey: string
            server: FastifyInstance
            token: string
        }
    }
}

import {ntru} from 'ntru';

import {Response} from 'light-my-request';
import {EncryptedBody, EncryptionPlugin} from "../../src/plugins/EncryptionPlugin";
import {Base64ArrayBufferConvertor} from "../../src/utils/Base64ArrayBufferConvertor";
import {expect} from "chai";
import {StatusResponseInterface} from "../../src/types/StatusResponseSchema";

import {getFirestore} from "../../src/plugins/StoragePlugin";
import {FastifyInstance} from "fastify/types/instance";


let buildServer = require('../../src/index');

describe('API registration tests', () => {
    let fastify = global.server;

    let key: { privateKey: Uint8Array; publicKey: Uint8Array; };
    let serverPublicKey: string;
    let emails = [
        "random1@test.com",
        "random2@test.com"
    ]
    before(async () => {
        key = await ntru.keyPair();

        serverPublicKey = global.serverKey;
    })

    it("Should succesfully send data to register API", async () => {
        let data = {
            publicKey: Base64ArrayBufferConvertor.encode(key.publicKey),
            email: emails[1],
            password: "123456789abcDEF"
        };


        let body = await EncryptionPlugin.encrypt(data, serverPublicKey);

        let response: Response = await fastify.inject({
            method: "POST",
            url: '/register',
            payload: body
        });

        expect(response.statusCode).to.be.equal(200);

        let payload = response.json();
        expect(payload).to.be.an("object");
        expect(payload).to.have.property("key");
        expect(payload).to.have.property("payload");
        expect(payload).to.have.property("iv");
        expect(payload).to.have.property("authTag");

        let content: StatusResponseInterface = await EncryptionPlugin.decrypt(payload as EncryptedBody, Base64ArrayBufferConvertor.encode(key.privateKey)) as StatusResponseInterface;
        expect(content).to.be.an("object");
        expect(content).to.have.property("status");
        expect(content.status).to.be.equal("OK");
        expect(content).to.have.property("message");

    });

    it("Should fail on wrong data send data to register API", async () => {
        let data = {
            email:  emails[0],
            password: "123456789abcDEF"
        };

        let body: EncryptedBody = await EncryptionPlugin.encrypt(data, serverPublicKey);

        let response: Response = await fastify.inject({
            method: "POST",
            url: '/register',
            payload: body
        });

        expect(response.statusCode).to.be.equal(400);
    });
    after(async () => {
        const db = getFirestore();
        const users = db.collection("users");
        const testUsers = await users.where("email","in",emails).get();
        const batch = db.batch();
        testUsers.docs.forEach((doc) => {
            if(doc.exists) {
                batch.delete(doc.ref);
            }
        });
        await batch.commit();

    })

});
