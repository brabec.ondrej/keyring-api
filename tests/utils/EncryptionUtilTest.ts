import {EncryptionUtil} from '../../src/utils/EncryptionUtil';
import {ntru} from 'ntru';
import {expect} from 'chai';
import {TextDecoder, TextEncoder} from 'util';
import {SYMETRIC_ENCRYPTION_MODE, SymetricEncryptionUtil} from "../../src/utils/SymetricEncryptionUtil";
import {Base64ArrayBufferConvertor} from "../../src/utils/Base64ArrayBufferConvertor";


describe('EncryptionUtil tests',  () => {
   
    it("Check key generation",async () => {
        let keypair = await ntru.keyPair();
        expect(keypair).to.have.deep.keys(['publicKey','privateKey']);
        
        expect(keypair.publicKey).to.be.an("Uint8Array");
        expect(keypair.privateKey).to.be.an("Uint8Array");

        expect(keypair.privateKey).to.have.length(await ntru.privateKeyBytes);
        expect(keypair.publicKey).to.have.length(await ntru.publicKeyBytes);
    });

   
    it("Encrypt and decrypt string(length 20 bytes)",async () => {
        const keypair = await ntru.keyPair();
        const testString:string = "Some Random Password";
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();
        
        let encodedString = encoder.encode(testString);
   
        let encryptedArray = await EncryptionUtil.encrypt(encodedString,keypair.publicKey);

        expect(encryptedArray).to.be.an("Uint8Array");
        expect(encryptedArray).to.have.lengthOf(await ntru.cyphertextBytes);

        let decryptedArray =  await EncryptionUtil.decrypt( encryptedArray,keypair.privateKey);
        expect(decryptedArray).to.be.an("Uint8Array");
        expect(decryptedArray).to.have.lengthOf(testString.length);

        let decodedString = decoder.decode(decryptedArray);
        expect(decodedString).to.be.an("string");
        expect(decodedString).to.equal(testString);
    });

    it("Encrypt and decrypt public key",async () => {
        const serverKey = await ntru.keyPair();
        const clientKey = await ntru.keyPair();

   
        let publicEncryptedKey = await EncryptionUtil.encrypt(serverKey.publicKey,clientKey.publicKey);

        expect(publicEncryptedKey).to.be.an("Uint8Array");
        expect(publicEncryptedKey).to.have.lengthOf((await ntru.cyphertextBytes) * Math.ceil(serverKey.publicKey.length / (await ntru.plaintextBytes)));

        let publicDecryptedKey =  await EncryptionUtil.decrypt( publicEncryptedKey,clientKey.privateKey);
        expect(publicDecryptedKey).to.be.an("Uint8Array");
        expect(publicDecryptedKey).to.have.lengthOf(serverKey.publicKey.length);
        expect(publicDecryptedKey).to.deep.equal(serverKey.publicKey);
    });

    it("Encrypt and decrypt of key with comunication key",async () => {
        const serverKey = await ntru.keyPair();
        const comunicationKey = await ntru.keyPair();
        const clientKey = await ntru.keyPair();

   
        let publicEncryptedKey = await EncryptionUtil.encrypt(clientKey.publicKey,serverKey.publicKey);
        expect(publicEncryptedKey).to.be.an("Uint8Array");
        expect(publicEncryptedKey).to.have.lengthOf((await ntru.cyphertextBytes) * Math.ceil(clientKey.publicKey.length / (await ntru.plaintextBytes)));

        let publicDecryptedKey =  await EncryptionUtil.decrypt( publicEncryptedKey,serverKey.privateKey);
        expect(publicDecryptedKey).to.be.an("Uint8Array");
        expect(publicDecryptedKey).to.have.lengthOf(clientKey.publicKey.length);
        expect(publicDecryptedKey).to.deep.equal(clientKey.publicKey);

        let comunnicationEncryptedKey = await EncryptionUtil.encrypt(comunicationKey.publicKey,clientKey.publicKey);
        expect(comunnicationEncryptedKey).to.be.an("Uint8Array");
        expect(comunnicationEncryptedKey).to.have.lengthOf((await ntru.cyphertextBytes) * Math.ceil(comunicationKey.publicKey.length / (await ntru.plaintextBytes)));

        let comunicationDecryptedKey =  await EncryptionUtil.decrypt( comunnicationEncryptedKey,clientKey.privateKey);
        expect(comunicationDecryptedKey).to.be.an("Uint8Array");
        expect(comunicationDecryptedKey).to.have.lengthOf(comunicationKey.publicKey.length);
        expect(comunicationDecryptedKey).to.deep.equal(comunicationKey.publicKey);
    });
    it("Text encryption and decryption (100 bytes)",async () => {
        const keypair = await ntru.keyPair();
        const testString:string = "eVEQncUAhVe YfMBu1ZrdLtbgX 4RAarImzSIQTxZSkA7Yl hU7V5XLoEune4NxqLg TA85ZdSbLw8u qeAQtOwP0GYnnroy5rwE";
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();
        
        let encodedString = encoder.encode(testString);
   
        let encryptedArray = await EncryptionUtil.encrypt(encodedString,keypair.publicKey);

        expect(encryptedArray).to.be.an("Uint8Array");
        expect(encryptedArray).to.have.lengthOf((await ntru.cyphertextBytes) * Math.ceil(testString.length /await ntru.plaintextBytes));

        let decryptedArray =  await EncryptionUtil.decrypt( encryptedArray,keypair.privateKey);
        expect(decryptedArray).to.be.an("Uint8Array");
        expect(decryptedArray).to.have.lengthOf(testString.length);

        let decodedString = decoder.decode(decryptedArray);
        expect(decodedString).to.be.an("string");
        expect(decodedString).to.equal(testString);
    });
    it("Text encryption and decryption special chars(100bytes)",async () => {
        const keypair = await ntru.keyPair();
        const testString:string = "E>M^{|#3Łs¦$jþ89'~'\€<Xi&Flo4s#t↓I#@c\|đ[vł€{&eJŧA9V2}bY[b¦7o<WsrG\Đ~xE¦^F$4|/y'G$←Md}}€xtl435`↓OcPT";
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();
        
        let encodedString = encoder.encode(testString);
   
        let encryptedArray = await EncryptionUtil.encrypt(encodedString,keypair.publicKey);

        expect(encryptedArray).to.be.an("Uint8Array");
        expect(encryptedArray).to.have.lengthOf((await ntru.cyphertextBytes) * Math.ceil(encodedString.byteLength /(await ntru.plaintextBytes)));

        let decryptedArray =  await EncryptionUtil.decrypt( encryptedArray,keypair.privateKey);
        expect(decryptedArray).to.be.an("Uint8Array");
        expect(decryptedArray).to.have.lengthOf(encodedString.length);

        let decodedString = decoder.decode(decryptedArray);
        expect(decodedString).to.be.an("string");
        expect(decodedString).to.equal(testString);
    });
    it("Text encryption and decryption (1000 bytes)",async () => {
        const keypair = await ntru.keyPair();
        const testString:string = "AHSmIegKZQovDNp3A0TvH5kQZKV9jix1CkPz hAjOUZ1etLwp5eLi7icYh42fwqL1hEAKeLAbk4dF4ZLlLjYQWBgWIFG9zsvYE5amTtNE1xf1dhCCh6dcpfQbJXP4FrQZ8PUss07JQh4xLDLt4N7I1DFtVTpA1RvrAtkwlEY7zZFEv67K86t1eEl4AIumz5voE2vxl5HajAJxwM1BWuZc59oVNqbXRyIEU2FRoYeneNBc4n x Rb03RZCRkpheFgcvBJde967oVIDbaoJJHVtP53dMFkhS6nV1yUvB95LHkOPB0ULMHdK51kwy06m465drPZFIs72QH0Ha55HRal9UkgvFZvf6i8T3DPNB8upwKpgFJ165kvi2rfStlFSyrGN5RM2V1I8dudBDNkUU6V9ReIPfdftJynLybTAiU4uC9hhSg6B6hRgVhXaZ7400WI4CtiRNe44Xyrg5lBgKdJqO5X3Onhhj31QNTyIOEl gFltbSZtI1pU483rkBAB4R60ERdVNLYvupEAGOrxJ4bH6z puBqc6PaKYqyvlckhsEv49bFTzJfkmRFFUGiULT0Zc1FHGnVNBPpVxa6iOqJ9A3rMGr8eqQBRvVIjxUHqgTfrnppJjKpgLrAXiy9gcHBwcAe0KXFqHvzZRKjjCRU22G0LLZlZ6o2ixHqlD ZqJgL02x7KUfoY9XiofUioGoKX1VP7g2xwIW36n1MoD6s64iwmyIfs0P5yOxUh3NgntkrVBYFaGNQ2xNIjbdMws2gT8K exHehpAJAbQgd8rDudIeaMNYLDnk76aD16WqX3e85DHidYfOnjQ02dt1pGx0Y6F48UqCZw05wOJaafzQCltjgQOJxYR3O1kLLjZd9BBnlYuRI81LeJinJkE bCNWei0ZSjJG5JmlG 1wVlkcYTzYPE3pTBv FYjMRqJFsDdUJn0SeKlK2hc6jiw BXUxRKvkLODyyd9gc tG4lSWqU3D6u30 mGp8Dv5Hpoxa 5t7";
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();
        
        let encodedString = encoder.encode(testString);
   
        let encryptedArray = await EncryptionUtil.encrypt(encodedString,keypair.publicKey);

        expect(encryptedArray).to.be.an("Uint8Array");
        expect(encryptedArray).to.have.lengthOf(await ntru.cyphertextBytes * Math.ceil(testString.length /await ntru.plaintextBytes));

        let decryptedArray =  await EncryptionUtil.decrypt( encryptedArray,keypair.privateKey);
        expect(decryptedArray).to.be.an("Uint8Array");
        expect(decryptedArray).to.have.lengthOf(testString.length);

        let decodedString = decoder.decode(decryptedArray);
        expect(decodedString).to.be.an("string");
        expect(decodedString).to.equal(testString);
    });
    it("Text encryption and decryption (10000 bytes)",async () => {
        const keypair = await ntru.keyPair();
        const testString:string = "IQzHlO0p5OfkVxqCsWc4VCEuzsUeBc41OYrHu28sDWu6dtpVqTr4P  sSfJoyRNsGptPzDz0GBvJwEybY4axvZufydpJO2uFdGvfmIBTcBSKKqJTzyo8EUMnlOvthZrzMlwpMWNH8QfQZf1uMCqCTmKjxriPDYDl4YDhJM9req9NpdXatHo0E2LHA1bPze WlopBFPGzRfRk5Seo9lIB55d09HC5wcR0AIPpjBgmcwpHV7qYHsJyfpsgD6MJC34t ClJJ1t2GB3YKXhv0cxXfluUMrewX3WOli8GvHRXwPXkIygvotNitKs8gln7 nwPyRTZG9cdt5zXxT3PxBVTW14zukm4STd6I0VdigUTkcw4sNAGohRWWCeA0tzIgmG5CctRsY0A1k9JDfJZPP6QrsMySYe5V7hsGfEqn7Ww41gyBY9HwoXHBoqup87skrxTttq72Qn3yCqYpJKIfWK9m6xKVGgl6WgndMJeC28xWYldpbzVZ8ODBMfmVTBx Yzi0E8fUo5QXTI9zDk6wetnpjnV65UAbHaX3BMQpyQT3OLZ2y6u i6Ncm7tU4EqhybajzOQKECKp0OKs 46fRjzM1fPVsFSo7itgA3zg 5mDsKMiCwDS6MspDtoZStqr3ndGXHwm3HcdAfDDcUAloecsnETMlIcm5Y ctrh2olD2K6ynlLSXMjpz8D TmN1MB1qzqzCQW6KtBhatins4LqQVGQmeczkZQ0tORlu kD0eUD9HTAv7vL5FupN1WMnSIMx6HmK 4OnNKrcJw7b6ydHLqJncWbCghu3jhnxuZzHRXwJmqtzRUGZFEeX1YwIKjVnAUY52FApb4WgNqic3X7elO1YjcrJvba5kvPUjsta T0U9SIwOapBmPBNR2KmpYui30Py7v5NiwHpR6n7nPINuEcwVbs1E2GCeBi1tVxLxJrSqlEJOi5 GTNFOwuI8EoytnRzgafp2jXAAiKNaqyTZqLdtssouTVp5RQogcxGuCGhvIiyFDcfVsMOP4y4VHHrbGFIvC0lrIPwseLI7bzTtRa6nLNMQ9Mxntmz9QvlLSi1vibQ7s3kT0Q1226gNefF4yf4yh2gDoWLCK2kuNuIdYXt4s01eIkEhFNxxSJxNWyc4w6KUHMVznm0EKTE9DpxewhM1bP9YTCh2lj60XCpavxXb7keuLlXrkXvmM2zOCRoBxT1ENdv 0rqH9E4GcaHNKUJwj7uRoubxCX41ygfXIUqg05nroXSbsIuv c2fbC7PnXX3F4sfPVkJiIrsGddcZHQKQoMd8r7UsQqLowSnczzoEJlQ6BLAb FlalfK0A5gUfPUNY528NTNeqojIgqfEqlKblMDk leXb iPY23hCtgeen7QrXxQQy0KT3RAlTKjwLFOsby5XvNFwAfqI1JZuKnZcQS4PW7tfsZGi6qqNM6CfyQG3aYPWB nUZxcb88bXdZGZiTTTi oScFDblJjHxhpG2P5m0dhfcyvughsGI0TkMpip9IcpL5MmC2lGIVppglWYBCg SnAS5FmnZhnOAR2rjDN6piQoIhJ6h77DQDod7anKt2RmG25gceHQtZ842xf x7b911JMjQx3Rl6KA51V6WfiPBj41XNlUYwPQGO45mK3qbE3byKVKLRgnL3JzrCYYI91n2FEWHquG5MrFa2nwvQjJAkH8LqfUL7ISgitGaRgdzwuh1BMdb7Ln6YuERa3n7lsrH6G4W65mkTqDK3oag0MYOplJ25lmWWXPHgh2DUXjpDoALmPHwLoNkw7JoXLo5jeokDJhADnwxcBt1mRRcJuWUvAtpjmzaiZwJsgqpOZjZJsltbaJH048XcArDuOwy9LOhs3Ytr2Q3u6E2pQDIpkPWVRmuwOqQex8RojBZ6Ezu8djpD7KPylkC4k4zkFnuekhkkV gaRUZKHf2assBNbOGIYIcxIwgtd3Ezn a01wovECr4kyYUd8l9QWwcwOdPCIxhC1N4KzbsqlGGXUlixVvJVCUHJvsv4MXxsoHGyqKWa8 rt8DY9PWVWOAB6DJFu4CVqxMdnNChINMSrTgjHjkAk202zxarny2pMAX r0pJCFJuwYIvTHhXHlYLeL0bO9MBmz9Uuq uMKHBNQPI17kgO27pRRMLDCWf jJB66IdW2g3qcy3wrl1z2I6BnZ6gNRcdkUczzpLfAtpMhjhNWlgco2ikCJG9AiltnsIuEJ8UZGnIsRkyzLVT6YoeCO2U10ZfFLROoTnvs3d01BIrb3v5TIcx6cagAV9pjzSbUCRRAmO8ri330g19TYDubR9SmtEYu4U8zRxbx9A5nUSBB8zE6neK6UNDvPtsxan8FcCZf0IKZiqnWyErhkilJr1QmiRCUfEvexMynaip3gGaWSvWgSwcrDBd9qNoYpjsJAMKhiYkDxJtzFQgh6ZZRWmYBZSBBosKwrgKJ1HgOjlRXbfn dZul q0pCHzpOgngarCmeumXbl2 ErKs9YrwvN7GRwsGcrlfPpk tVZvvNVaEBp0OlC5sEr2AgRajhUU7 gJV8GFJweuQ8eASbKo57Icag2W1b2RMvoOXnZ74nSYLKHVDOERS8a2b 3UoJq8IUejqS48ap2D5jgFry3AmcLAngwVvc8LXM2FXHJuiH5hdXzY0qNnUxKpLebRw3jROlJC0E6EQcHbaFt29 E1ex5VhkWY2j8PF8FfSfntFRyUCHsjPuNxYcLjc48BbR9nddRR6tV65pd892Ux8jCEQindWHDm4vC6ESgzFIFbBRf5CiWWBzRpUwBS twp2bPZH45fOpgEBEMP1hHTvEs0gz4n ewkVwOnuCCHVR0IhPU8FYRwQhCMu748vpS37Q5CNNI9EFkAqxowj6Cpq9KeMg04s6lolE9GzytlMjl0t32igZbcSmw7IcY5KfkIkJP6TOae160b0hDtluE7vjh3JfEfRbpMWAOfxQY VeUQ4YyVKIpV4NTw9eg0hDjxaz5V0qk31xXGBQOKIrphr2kgoKTnkQhYnEhWLwj8YMqBxW1ru2CF7g36iT2F4E3iZPYHJWo2PKq5kfAtMYKSOiubtdMjvex0Sj9iCnDHIXg3RXxehEnxHj6ezfy9eOHfOLmO88ADyK12jDjMDzn9jdh5MqAXxhN9Fd77ZKDhb3DeFc85pxwKiadgWPGSpqfTOZb1IxZdwRjBQS4xTNq7nCBtPZi1ZN9ipmEwdaWBkOYbq NCa3pepJ0mL6InZcLKmprkuXiRroYPW iQ2pRdPo11Rjss4j8eauTGdC4VkxiNzytk2ohN5AFS8Hk86ubtuJJ77Ss1ijR8y9PQ84UO1 GmtLtJPSGztoyA3uUHnZ3a6prBwoy6MszCtp5nbyuBJz1FGnSa64MWjDXMkVphp8WWg1Rr0PCHiSoCYP1j8UxdCOQvTuiNcE55bFx7VIfsNWJp1AwO8y5161ODEcJCxLAMUWXTZjMhluUJuvKrE6ZpNeh1QowRCK4T4d mpQjK8l0bnJD9UmXSQ7ATDub4FMLR44tqXdVnABDDtmOmFRthn7iOrdaHHnvwZo5Jwo1Yc 4PtBWczR6mmG2Ur2jTMMFaw2Z29Zf sx5SOE 1aCXCmoghZR2XhQJfNzIdyHVMOeJI9719YWwGkpjnfmSuECOZYkSjF8oZp7JS8CXzS1WeOyHQQh8Lrju0OrG03YLzWHmC65N0WYioMghf ZRyfKl5OcEZC3onxIfaAxqjbymQ79qA274gCzCPTMi5AYyBfhunnhIHeZDm3CTOt8E ud5lhoEqgL6tUJeNFKJW43icxZ4E2EhK5hQ51KWBUE305jd  nWLScM4L7aMOtOrtBUCu0NuhxYlfg248XPJSUPp7V9sseMmX3dciDcj2pEUteTRe5NRV8SBpoFRm A6xTgxWOTDXl58nxxHchklsmqrdM9Ixmt8X0gNfCvt xOogckXLYdhPG7ind0hpcfrncnIG1IH1YBNOfA3LGMylIreUA2ul6zO xIEWog0Wfx sRtESvZlVJrd4maP434Tj7bWDp8riLsIKuqt4m9ajHFfTup2Y4cGJGwIDaJqxcvkBUbLIFPzmXZqfHPjlBMIrVkp7OPIUihkKZ811RWL8tOqP5C5xLOt3IOXzJQ46M5JXvpAaaaAV9DprdKI4EWtNjjk7WWZ563cZku1vZAwwoQWHXK4y7285B9iv7aPHXsYNvF4mMUzhm9CdWazqYeKUD3zgGzmKh6QEdztbJBvqDUWI9b193YQp7Wn0l41WP3ab0YGwbndDngz6Q0G0YnTrJjGRqNHr8UtNPa2znlGX3kAAdfwjK2I5r6SQfGgjE2POSvnDLqek7fbNCsLPUWi5oZSxeuuWy3xzx7z64V E RFnYspJlD8PcNqsQG9G9Vd968kSAUTG6KY0GX1VcW6XLSoRWFD4pnabKWchu2U JjS5GBKadM u6t6vJQ7Ui0NhJ0QQi2USR8QPc7nvFXIOnu0SeANmVfOoQEj1hugJ03G2kZigJ5nzlZsXgEXOQY70NIVbZd8DxJdanx4tsCF5pNsbjb9fqw3lET76z5QfHKaIsQpockGFf4UO9zqS HWBktNEFiloTeqzGhphxswyUsH9 8AEWEbs RYT5F0DmdgnpawaJA2o2lokDpTdftGobNdcduhzveLGcJ1EUOMZ Eq2USsJX5je93xkvvNhQJu2Jza3jlSgLhrOF2IZVsTo4tOT4Ap4ieSWMNd08Vdx2ruQL2c6NQfTejWqh8kvtGpmMwGflLVtMLh56WRBK9No0w nbcyQSd3AThSkCL12Q4KfNRUnJRyaF4WvJlIxKotZ49jlWqnXqGuqRiITwdRNk2EIxN9u5ThPIMAB29 7 tPzbEtcSomFkqae8krADOsrKRhSmwJISNU0 ao7H3HvPXyq4tcJ iHFF4Tzva130FZyMbscoezChSqE943bVEShItTuNwwR9nt4QRM 1M2zWVwDpRJfRbELTMhUq7xAaaPQSWL nZ6iXySG1G9GYZDKLSIiBgnV3 UukXKUlvAhSv1By5uRNkYR8I4BFP0ajuMooQuoajtgoB4Ahj9kJbS6begZQUHI4tiOPTYBmt3tB5oSecy7Y5QUhFznDwThmXWhPC5FLxM9RrtTM1O0ex1zEdwKgxOrvRVYoamRAJo44A6DHT8dLRiCleDv3iGSfagNnX6auW8DxB a1kmMXhcelgW3v8UDfacnjnkf NNBR4USMdz0Ove0gBz84pjnJNIx5mdkGDHLnHpA9ayelntec1CUvEXSGsUBIulbYCyNbvteCCEzdNG4CGhh3SiDu3zw7UdIxMSNIYl0Nn5ZxqV26nl1xbVZrclMP4AxjOHFu2eEOyJdjwQ9SmTqo29SICcevG1qGhFpL0wJPWPdzUcN8YOZ1HJQ iAAbzqkH8lb7PN2J57nuuSCznjypAszLxVvbr1G0wyooANRwSUryrxviedkQwFo5kw1jCebrenSjEF5vhgWbFXLcZubRLt6bhhbEk4PONKyVihCIRy2aAmzatTU23GrMdYozvyonnWlPv6o2vTW4TWvFx6O4wSxR6CoQiAX5IeS5MkFFT7suh5a0eQCSoWHHSK7QsMEJD5CVGrWEfLlctcvUjONaaBJzPwK8QmKvbrMUTQngzrDbY1pvnm TkEJRkhgwXPe yw95rHtoAKvOESU20eTJO6ufF0pP wzFURC07itLzQ2v76RWZR5SfokGrqUPUdXAepWNa8hHuNsTEO14uOGqwH0aY3a7v  ivfoObnWJG4rKYnT9go8WpK8dt1m 1ulXf8MyDpRmGrYAMGso52ENcYV4L2AY1yAHRB86eDXuQHw0dH6PvFb5K83m0teYpjO6vHH7e aXeVi8zyvM99XbrvsU7 TMzSsyOpfs1cCp1JjbRxSK7VdpX4UHLOKjoJgHahMA1FvN4PG WHFDau0s132JBWceQhX16tqm6gF7AuQBZCCzYQTkjmkGIf8H5seNII6kwQAcjSDGdXnWbo4tckAKgddjGdgFMNJLuPBpGWd13Bpo1Jp2MIzgoNAo0UhgsgqQti6XLys4wTQQwdpBTbQ9Q2Ji2sysZ1j0 TVetjZ2fokzz8WMdhLDQ5buCiJ YuzDuDSUY78Rz7EW18BAEG77VS6oj5DoEzFmBoXeOJHYWChnR8tIhc2uAsKg7evjy8sPh4eoKHCMoAFJNXVeIgZrAHykZVbBjdTzFdL774HWD4kfOmtVMV4xrKovqfhO9f0yrPmayYq6Tdeg8IlLWckEoy9eBisHD8iYvu3cLByniisBBZWUDQK1hVqateErwa301olvutYMghPQrWuHLjROZuU3XaSEGJW7j4BMtHlAayDjXG pepZIJosfdtOY mUSaPcpG6bAX4JlLFFRDxLSeBExBIIcX Utpn3cCMPKHOSup3TDRz 4zm7ga2STmezcrGGYLa87F6SRrATctSpDbNRMYDTQO8znkEnwjaDWkGD2ugQxGzNcFW7N6wSzTeWQ0 w6k1mVsiOPUFetMeTTdrGKplFuiMPy9Fh5t1fkzSQI91458GZKXvR6N25mKX5aEs2mKuGsFv7ueGnbyUOLRpa lsp55UmEouI BQDvydfcrzkn7ZBNDHj43f 424ZFykaoU24WE3Aq8hgK9yjC2V1cvuW1eMAY4kTo5ytuAQatGbrsnUc0p50WXBc6AaOcwMP96FTMGBbgEBnKSE9aNKpZpy MRvBHNBFfm4B9HWJCBIygHB8BNfcXzOoDRzNhqyQI88RXGeVOBdAKB0GIJehnSzd9RK2jtjf3ZCGB6AgYgIiUs d0TxANvtHVNoYOfJApIA gO5Ep8dKoHBOTab8Dtx2hEq3Cs7C6LykT5QZCM9FLhag5KvRpgc53Lf3SEp12ZG0KLqPRrz1rDcTBoyig9TINmSv qOxSF8qZaYeRAC7NiKLCpud2RFqfTW JwCvgOUx122ZbjP2Rj8l7eI1oiDHLFKPIyT5U6xL3hz5 IgqpvhGUgFVb7MYif7QwjOUuohmDtc84ZEtnKtgviYnrwAqJJJHXN2JSvP YrA1C8SdkgQaXZJeMrfpAmcVjL peg t5hbEN87lYcR1TM82Dh82Hx6mfIECUlDDhvevdD2vFWWMNSwRuJQ9ZmA0RUax0tCUGr9yWFrm4AeoIZa3Xo8vVqIaC10QDd3HcN6zFQvECPyi2vEwfx5509QzZRLr7W3ebkJxZiT98 nSt6nBfSAQTfftKHUh0kYBXw0fliA2QKZOt6Y6jOMRMwVsWV0QwY6yK5PfMRuoWt9s1eI8w8sLOn7nhqO4KRotqHRv9B76 iST RaZKYt86eUcH39Or9gtkwZ7owUmclzzFT9CHrfMCyWPF6cEftcxBTTpkWL0BNrA7dVyfHJQGvqvCVoxmSmB6ykEUraChpLG0UUalCczdEfJC6BrxWW3DavHbjeX1DuSMFCbMAnh9wMETgAku940QVS5dEFBz1OAxnwDKrufeVcjPtU3gF9FJpz2zqSfOE4SwdwNSXt vICy1hzJt3doowVveBsQIJ7jCtEaAt8mNgPDpB0KEnWbNQW2FioMWuqU2QUelm2Vo0U98Kz0h5YcSNXkHQrTSlo5jIclDqWMEZDsfenuftkfuJLyZ2y2ZEVSEBr7NDueWWDo7JxbvqeD65RScOpVeSlRASfBWSG8XEZOMeuihrFXD0fvHT2LO7aMHr1FW3Jb6g4iMppk9cyTC6WLhU040y078G3esAO8f9GLzvuyOwFFBJ6sgG4uOu2EqLO2V8T1KTr4finc21WZNG1MKua pN9pbDbq3j5LTOPkoa5UN2Cs3gPh24DV3vDtQlQU83eB3hYQKZcLaB20vuZ6 nC18X8LpGF6rI5xwVdeeH7Vx99t5Ss2SJB081T18fEeStysoAm71 Mg4dXKF3vLuvg5ecWKbrAEjjgqoSP520WT3HMixTcKK0L89apa6lbdG0VA7x0DZOI W4dW2jzSeyt7pWIDOkzRDFbG  yGqSZA7vPyEKfxbVkwRo6r4US7rKb9HCEhu5wubGRGTMoWLOczcOtZOUH4f5H2afhgA3pTYH9nrrFLrANwj56WUtQ76TE1yMle DRmq2D2SRlHvh3huIIbUBMKDFb4PJVaQUuTl3fcNzRRGC4p9OzKY52oLgDKHeR2oGGXt4O22gC2HePc7psRNiJH PDOkHQvJUiEkIqm VCX9GAHBCYk1DulCW2u6t4hkeazgz2E51kdmZW1LGjx4aVz8BmbOhmIW3AA6i8MKzufHUuBjDMwe4Pi8CtPHus3qwW4U2RmjM5g52zgdc4pfX4lAoeLUI9B0OPlv5N R6Oaft3WzroM6Pse3LpocpNDS3rILEEbuNkWcrr8DSkZo9PM06gpkYn r88jq6GdvAs22Ek7WwghheKPIctFWrK ITMiIkpL9DehJ82bOQsF1OibnVqxYWH4n W4jWCW1paRDe9PRTGztU6uD1PUNf4EZmCWVuLyxuyjQ5s0hbPvT3mD6gcqFW0ZpkzMgYEun1I6aO3H 5Ja2wZzEEoR346S uYAHfnFnWtGIi0hnEVycbIJIRrbwFZeAGhE3f Go Fl  zQtGxN8k8P4bH8jG3EwaraEIK4KOyiJxfwqyGVK339aIajQk8NmVMZiIKjRgoPNdX gmedQyCIvPt6 riVSUnuA3iPFYZIhBsXi76dHmat6 xrBAMZlGkX0pInJMmXme rfQSJxri9sSXq24GC3c 4IpG95WOVfyYR 4ejq4ayke2FRD7ZpTBrN6qvVS8EhZv5rNroXSuU89FB2hxQtna1Yj5wXPp0k6sLm3y6o6GugBFenceqZVBla63vtLsGGKngqiRdMugjD3pL4FkBvMvm1uvwI0Y7bmz0smwSzveWWoCUwwGFZ OReA8xsgRpsExoDoT30lz8Xcya38ljFOsb7oErNd08puakJNVjH4oJLUPZGb0qXdHng1THqcRUzFBIqSJoQlgHQhe03HaYHa7lGita7h5dllwAnuvqO9tKnOnXwk4KjmhILKbP7oVQR7i6tDoVGPgUP78W5ehxbgW79yBxZOFpHkNruA7n3JGFsbiXjZuuPqPqH53AuNe31sPcXA2e742pCpzFpjKG1Ih5sNtk8xA2WBQB2f9W7u9zccxQloQ8dFU4JTMXsvtFLzTlq1vQyFhfD96bk81okIZHDvIydQX0Whw41X1vYFNonIa8asmzVtDhR0mNt9Kht8gUu9tGXtwdpw6WF8M9wpEQ05q7rwrBPYepOKaKnnl9Ozs9R1nMNKY5mFOs4T2cYNAVWtgDLN PMbH25tgSF5h eOH4cSerRVh5wtpchJZK6upiZSf5zk6BLwkEKuzcFXw3PRMiT1WQR8KAgEIEZOAcv5LBqoISxj4fKtWrCOA ANzunKxa6ah3gwO9lVg0gCYWQVAixrNxZ7WJTU5mXspXT3LQrbTcJg Sp4EnLaZfOaFjTZV0PLPK6 mWXf6Y8qrPE7zVTU7 xnJfoZANl2OwTTQNr4viz1r7HSUg0nUc9b9j1iSz3S0xHYZ6sPsU5qsc2WOqW4POwdGgcmKsRKLJcVJTLJfyzZjPcXmjySTMWjnUzF9WeErTP93HNzxT64Haah6vpuvFs9qg20 nDSK3hVjNHpSUAujHTYn8nBkV6VlV7W0vlMaRL5Gktj8oSNxRb2X5esTI 1lA25a8iajYWcJmHZ6RUqVkAkE4Sl1G8H6PMVjWI2q6BnxLMVdy0VtLW16kLMjUmgqa8QObysWgpCWy7Xl9LnqfAAJ0S77ec8YvmlatcQ3clV1GSx1OkkikhbmHL1OtkDwllCzVmhn5KzoMY3q6ODg2MiDVgUwDhFfqzYpk6ZJtbRuMym9fgzH0o2G8Rcl5Xia3MpNOtlfZbhNWnjQQpsDGoUYQL2nRN8VSpKGbZZAfj99MOlI0Yke5ZpUieQTGB2XjFH32uV7p2ZxIYY6i72NaSiWXvFtXdkcQImgSCAIPGoJW0eM9VAUZYjo";
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();
        
        let encodedString = encoder.encode(testString);
   
        let encryptedArray = await EncryptionUtil.encrypt(encodedString,keypair.publicKey);

        expect(encryptedArray).to.be.an("Uint8Array");
        expect(encryptedArray).to.have.lengthOf(await ntru.cyphertextBytes * Math.ceil(testString.length /await ntru.plaintextBytes));

        let decryptedArray =  await EncryptionUtil.decrypt( encryptedArray,keypair.privateKey);
        expect(decryptedArray).to.be.an("Uint8Array");
        expect(decryptedArray).to.have.lengthOf(testString.length);

        let decodedString = decoder.decode(decryptedArray);
        expect(decodedString).to.be.an("string");
        expect(decodedString).to.equal(testString);
    });

    it("Should encrypt symmetricKey", async () => {
        let symmetricUtil = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS);
        let keypair = await ntru.keyPair();

        const testJson = {
            "it": "just",
            "works":"Fine!"
        };
        // Start of encryption
        const string = Buffer.from(JSON.stringify(testJson),"utf8").toString("base64")
        expect(string).to.be.an("string");
        let {key,encrypted, iv,authTag} = symmetricUtil.encrypt(string);

        let stringKey = Buffer.from(JSON.stringify(key),"utf8").toString("base64");
        expect(stringKey).to.be.an("string");
        let encryptedKey = await EncryptionUtil.encrypt(Base64ArrayBufferConvertor.decode(stringKey),keypair.publicKey);
        //End of encryption

        //Start of decryption
        let decryptedKey = await EncryptionUtil.decrypt(encryptedKey,keypair.privateKey);
        let decodedKey = Base64ArrayBufferConvertor.encode(decryptedKey);
        expect(decodedKey).to.be.an("string");
        expect(decodedKey).to.be.equal(stringKey);
        let explodedKey = JSON.parse(Buffer.from(decodedKey,"base64").toString("utf8"));
        expect(explodedKey.salt).to.be.equal(key.salt);
        explodedKey.secret = Buffer.from(explodedKey.secret)
        expect(explodedKey.secret).to.be.deep.equal(key.secret);

        let decryptSymmetricUtil = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS);

        decryptSymmetricUtil.setKey(explodedKey);

        let result = JSON.parse(Buffer.from(decryptSymmetricUtil.decrypt(encrypted,iv,authTag),"base64").toString("utf8"));

        expect(result).to.be.deep.equal(testJson);
    });
})