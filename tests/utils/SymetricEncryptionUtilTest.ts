import { expect, should, util } from 'chai';
import { it } from 'mocha';
import path from 'path';
import fs from 'fs';
import {SymetricEncryptionUtil,SYMETRIC_ENCRYPTION_MODE} from "../../src/utils/SymetricEncryptionUtil";

describe("Symetric encryption and decryption test", () => {
    it("Check password encryption and decryption",() => {
        // Prepare Two unchained objects for encryption and then decryption
        const utilEnc = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.USER_PASS);
        const utilDec = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.USER_PASS);

        const text = "Some text i need to encrypt";
        const password = "SecureP455w0rdF0rTheW1n"

        utilEnc.setPassword(password);

        let {key,encrypted, iv,authTag} = utilEnc.encrypt(text);
    
        // Test on result types
        expect(authTag).to.be.an("string");
        expect(encrypted).to.be.an("string");
        expect(iv).to.be.an("string");
        expect(key).to.be.deep.keys(["secret","salt"]);
        expect(key.salt).to.be.instanceOf(Buffer);
        expect(key.secret).to.be.instanceOf(Buffer);

        utilDec.setPassword(password,key.salt);
        let decryptedText = utilDec.decrypt(encrypted,iv,authTag);
        expect(decryptedText).to.be.an("string");
        expect(decryptedText).to.be.equal(text);

    });

    it("Checks random key encryption and decryption", () => {
        const utilEnc = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS);
        const utilDec = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS);

        const text = "Some text i need to encrypt";

        let {key,encrypted, iv,authTag} = utilEnc.encrypt(text);


         // Test on result types
        expect(authTag).to.be.an("string");
        expect(encrypted).to.be.an("string");
        expect(iv).to.be.an("string");
        expect(key).to.be.deep.keys(["secret","salt"]);
        expect(key.salt).to.be.equal(null);
        expect(key.secret).to.be.instanceOf(Buffer);
     
        utilDec.setKey(key);
        let decryptedText = utilDec.decrypt(encrypted,iv,authTag);

        expect(decryptedText).to.be.an("string");
        expect(decryptedText).to.be.equal(text);
    });

    it("Checks JSON base64 encryption", () => {
        const utilEnc = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS);
        const utilDec = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS);

        const data = JSON.parse(fs.readFileSync(path.join(__dirname,"bigjson.json")).toString("utf8"));
        
        let text = Buffer.from(JSON.stringify(data),"utf8").toString("base64");

        let {key,encrypted, iv,authTag} = utilEnc.encrypt(text);


        expect(data).to.be.an("array");

        // Test on result types
        expect(authTag).to.be.an("string");
        expect(encrypted).to.be.an("string");
        expect(iv).to.be.an("string");
        expect(key).to.be.deep.keys(["secret","salt"]);
        expect(key.salt).to.be.equal(null);
        expect(key.secret).to.be.instanceOf(Buffer);
        
        utilDec.setKey(key);
        let decryptedBase64 = utilDec.decrypt(encrypted,iv,authTag);


        expect(decryptedBase64).to.be.an("string");
        expect(decryptedBase64).to.be.equal(text);

        let decryptedObject = JSON.parse(Buffer.from(text,"base64").toString("utf8"));

        expect(decryptedObject).to.deep.equal(data);
    });

    it("Checks big JSON base64 encryption", () => {
        const data = {
            col: 1,
            text: "Some text i need to encrypt",
            float: 0.2555
        };
    });
});