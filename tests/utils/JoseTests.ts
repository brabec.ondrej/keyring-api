import {JWE, JWK} from "node-jose";
import {expect} from 'chai';
import {Base64ArrayBufferConvertor} from "../../src/utils/Base64ArrayBufferConvertor";


describe('Jose tests', () => {
    const keystore = JWK.createKeyStore();
    it("Should be able to generate Keys", async () => {
        let key = await keystore.generate("oct", 256, {alg: "A256GCM", use: "enc"});
        expect(key).to.be.an("object");
        expect(keystore).to.be.an("object");
    });
    it("Should be able to revive the keys", async () => {
        let key = await keystore.generate("oct", 256, {alg: "A256GCM", use: "enc"});
        expect(key).to.be.an("object");
        expect(keystore).to.be.an("object");
        let exported = keystore.toJSON(true);
        expect(exported).to.be.an("object");
        let keystoreNew = await JWK.asKeyStore(exported);
        expect(keystore).to.be.deep.equal(keystoreNew);
    });

    it("Should be able to generate jwt token", async () => {
        let key = await keystore.generate("oct", 256, {alg: "A256GCM", use: "enc"});
        let data = {random:"data"};
        let encoded = Buffer.from(JSON.stringify(data),"utf-8");


        let jwe = await JWE.createEncrypt({ format: 'compact',zip: true },key)
            .update(encoded)
            .final();

        expect(jwe).to.be.an("string");

        let decrypt = await JWE.createDecrypt(keystore)
            .decrypt(jwe);

        expect(decrypt).to.be.an("object");

        let decoded = JSON.parse(decrypt.payload.toString("utf-8"));
        expect(decoded).to.be.deep.equal(data);
    });
})