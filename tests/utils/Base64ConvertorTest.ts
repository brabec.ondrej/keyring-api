import {Base64ArrayBufferConvertor} from '../../src/utils/Base64ArrayBufferConvertor';
import {expect} from 'chai';
import {TextEncoder,TextDecoder} from 'util'; 
describe('Base64ArrayBufferConvertor tests', () => {
    it('Check basic convert',() => {
        let testString:string = "Test string for encode";
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();
        let arrayOfChars = encoder.encode(testString);
        expect(arrayOfChars).to.be.an("Uint8Array");
        
        let base64String = Base64ArrayBufferConvertor.encode(arrayOfChars);

        expect(base64String).to.be.an("string");

        let decodedBase64 = Base64ArrayBufferConvertor.decode(base64String);

        expect(decodedBase64).to.be.an("Uint8Array");
        expect(decodedBase64).to.deep.equal(arrayOfChars,"Wrong decoded array");
        
        let decodedString = decoder.decode(decodedBase64);

        expect(decodedString).to.be.an("string");
        expect(decodedString).to.be.equal(testString);
    });
});