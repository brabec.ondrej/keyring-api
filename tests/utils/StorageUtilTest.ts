import {expect} from "chai";
import {getFirestore} from "../../src/plugins/StoragePlugin";

describe('Storage tests', () => {
   it("Should connect to Firestore",() => {
       const firestore = getFirestore();
       expect(firestore).not.to.be.undefined;
    })

});