import {getFirestore} from "../src/plugins/StoragePlugin";

after( async () => {
    const db = getFirestore();
    const users = db.collection("users");
    const testUsers = await users.where("email","in",["random@test.com"]).get();
    const batch = db.batch();
    testUsers.docs.forEach((doc) => {
        if(doc.exists) {
            batch.delete(doc.ref);
        }
    });
    await batch.commit();
})