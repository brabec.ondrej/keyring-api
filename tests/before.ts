import {FastifyInstance} from "fastify/types/instance";
import {Base64ArrayBufferConvertor} from "../src/utils/Base64ArrayBufferConvertor";
import {EncryptionPlugin} from "../src/plugins/EncryptionPlugin";
import {Response} from "light-my-request";
import {ntru} from "ntru";
import {it} from "mocha";
import { expect } from "chai";
import {LoginResponseInterface} from "../src/types/LoginResponseSchema";

declare global {
    namespace NodeJS {
        interface Global {
            serverKey: string
            localKey: string
            server: FastifyInstance
            token: string
        }
    }
}

let buildServer = require('../src/index');

global.server = buildServer();
before(async () => {
    let key: { privateKey: Uint8Array; publicKey: Uint8Array; };
    key = await ntru.keyPair();
    let tokenResp = await global.server.inject({
        method: "GET",
        url: '/token'
    });
    let serverPublicKey = tokenResp.json().publicKey;
    let data = {
        publicKey: Base64ArrayBufferConvertor.encode(key.publicKey),
        email: "random@test.com",
        password: "123456789abcDEF"
    };


    let body = await EncryptionPlugin.encrypt(data, serverPublicKey);

    let registerResponse: Response = await global.server.inject({
        method: "POST",
        url: '/register',
        payload: body
    });

    let login: Response = await global.server.inject({
        method: "POST",
        url: '/login',
        payload: body
    });

   let encryptedBody = JSON.parse(login.payload);

    let loginBody = await EncryptionPlugin.decrypt(encryptedBody,Base64ArrayBufferConvertor.encode(key.privateKey)) as LoginResponseInterface;

    global.serverKey = serverPublicKey
    global.localKey = Base64ArrayBufferConvertor.encode(key.privateKey)
    global.token = loginBody.token;
}) ;


