import {randomBytes,pbkdf2Sync,createCipheriv,createDecipheriv} from 'crypto';
import Firebase from 'firebase';


export class SymetricEncryptionUtil {
    private readonly ALGO = "aes-256-gcm";
    private readonly SALT_LEN = 16;
    private readonly IV_LEN = 16;
    private readonly KEY_ITERATIONS = 10000;
    private readonly KEY_LEN = 32;

    private mode : SYMETRIC_ENCRYPTION_MODE;

    private key : Key|null = null;

    constructor(mode: SYMETRIC_ENCRYPTION_MODE) {
        this.mode = mode;
        if(this.mode=== SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS) this.generateKey();
    }

    /**
     * Function to set key by transforming user password
     * @param password
     * @param salt
     */
    public setPassword(password:string, salt:null|Buffer = null) {
        if(this.mode !== SYMETRIC_ENCRYPTION_MODE.USER_PASS)
            throw new Error("Wrong mode called");

        this.key = this.pbkdf2(password,salt);
    }

    public setKey(key: Key) {
        if(this.mode !== SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS)
            throw new Error("Wrong mode called");

        this.key = key;
    }

    private generateKey() {
        if(this.mode !== SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS)
            throw new Error("Wrong mode called");

        this.key = {
            secret: randomBytes(this.KEY_LEN),
            salt: null
        };
    }
    /**
     * Encrypt string data with AES-GCM
     * @param dataToEncrypt
     */
    public encrypt(dataToEncrypt:string) :EncryptedResult {
        if(this.key === null) throw Error("Missing key");

        // Generate random IV
        const iv = randomBytes(this.IV_LEN);
        // Create cipher object
        const cipher = createCipheriv(this.ALGO,this.key.secret,iv)

        // Encrypt data and transform to base64
        let encrypted = cipher.update(dataToEncrypt, 'utf8', 'base64');
        encrypted += cipher.final('base64');
        // Return all data needed for later decription

        return {
            key: this.key,
            encrypted,
            iv: iv.toString('base64'),
            authTag: cipher.getAuthTag().toString("base64")
        };
    }
    /**
     *
     * @param encrypted
     * @param iv
     * @param authTag
     */
    public decrypt(encrypted :string, iv:string, authTag:string) : string{
        if(this.key === null) throw Error("Missing key");
        // Create decipher object
        const decipher = createDecipheriv(this.ALGO,this.key.secret,Buffer.from(iv,"base64"));

        // Set auth tag after transformation from base64
        decipher.setAuthTag(Buffer.from(authTag,"base64"));

        // Decrypt given data and transform it to utf8
        let str = decipher.update(encrypted,'base64','utf8');

        str += decipher.final('utf8');

        // Return string
        return str;
    }

    /**
     * Transform string password with salt(or random salt) to Buffer
     * @param password
     * @param salt
     */
    private pbkdf2(password:string, salt:null|Buffer) : Key {
        if(salt === null) {
            salt = randomBytes(this.SALT_LEN);
        }
        
        return  {
            secret: pbkdf2Sync(password,salt,this.KEY_ITERATIONS,this.KEY_LEN,'sha256'),
            salt
        };
    }

}


export enum SYMETRIC_ENCRYPTION_MODE{
    USER_PASS,
    RANDOM_PASS
}
export type Key = {
    secret: Buffer,
    salt: Buffer|null
};

export type EncryptedResult = {
    key: Key
    encrypted: string
    iv: string
    authTag: string
};


