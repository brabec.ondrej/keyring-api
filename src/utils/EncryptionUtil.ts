import { ntru } from "ntru";

export  class EncryptionUtil {
    /**
     * Encrypt text with NTRU
     * @param text
     * @param publicKey
     */
    static async encrypt(text : Uint8Array, publicKey : Uint8Array ) {
        // Prepare available byte lengths
        const plaintextBytes = await ntru.plaintextBytes;
        const cypherBytes= await ntru.cyphertextBytes;

        // Prepare Uint8Array with right length
        const cypher = new Uint8Array(new ArrayBuffer(Math.ceil(text.byteLength/plaintextBytes)*cypherBytes));

        // Go through plaintext per parts
        for (let index = 0; index < Math.ceil(text.byteLength/plaintextBytes); index++) {
            // Get part of text
            const part = text.slice(index*plaintextBytes,(index+1)*plaintextBytes);
            // Encrypt and add to cypher array with right offset
            cypher.set(await ntru.encrypt(part,publicKey),index*cypherBytes);
        }
        return cypher;
    }
    /**
     * Decrypt text with NTRU
     * @param cypher
     * @param privateKey
     */
    static async decrypt(cypher : Uint8Array, privateKey : Uint8Array) {
        // Prepare available byte lengths
        const cypherBytes= await ntru.cyphertextBytes;
        const plaintextBytes = await ntru.plaintextBytes;

         // Prepare Uint8Array with right length
        const text = new Uint8Array(new ArrayBuffer(Math.ceil(cypher.byteLength/cypherBytes)*plaintextBytes));

        // Go through cypher by parts
        for (let index = 0; index < Math.ceil(cypher.byteLength/cypherBytes); index++) {
            // Get part of cypher
            const part = cypher.slice(index*cypherBytes,(index+1)*cypherBytes);
            // Decrypt and add to text array with right offset
            text.set(await ntru.decrypt(part,privateKey),index*plaintextBytes);
        }

        // Filter null columns and return
        return EncryptionUtil.removeEmptyArrayEnd(text);
    }
    /**
     * Go through array from end and remove 0(null) values
     * @param {Uint8Array} array
     */
    static removeEmptyArrayEnd(array:Uint8Array) {
        let count = array.length;
        while(array[count-1] === 0) {
            count = count-1;
        }
        return array.slice(0,count);
    }

    static async generateKeys() {
        return ntru.keyPair();
    }
}