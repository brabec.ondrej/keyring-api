
export class Base64ArrayBufferConvertor {
    static encode(arrayInput:Uint8Array) {
        return Buffer.from(arrayInput).toString("base64");
    }
    static decode(stringInput:string) :Uint8Array {
        const buffer = Buffer.from(stringInput,'base64');

        const ab = new ArrayBuffer(buffer.length);
        const view = new Uint8Array(ab);
        for (let i = 0; i < buffer.length; ++i) {
            view[i] = buffer[i];
        }
        return view;

    }
}