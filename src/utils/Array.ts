export class ArrayUtil {
    static search(arr: Array<object>, s: string, key: string) {
        let matches = [], i;

        for (i = arr.length; i--;)
            // @ts-ignore
            if (arr[i].hasOwnProperty(key) && arr[i][key].indexOf(s) > -1)
                matches.push(arr[i]);  // <-- This can be changed to anything

        return matches;
    };

}