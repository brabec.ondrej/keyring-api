import Fastify, {FastifyInstance, FastifyLoggerInstance} from 'fastify'
import {Server, IncomingMessage, ServerResponse} from "http";

// import json schemas as normal


// import the generated interfaces
import {AppModule} from './modules/AppModule'
import {PingModule} from './modules/PingModule';
import {TokenModule} from './modules/TokenModule';
import EncryptionPlugin from "./plugins/EncryptionPlugin";
import {RegistrationModule} from "./modules/RegistrationModule";
import StoragePlugin from "./plugins/StoragePlugin";
import SessionPlugin from "./plugins/SessionPlugin";
import HashPlugin from "./plugins/HashPlugin";
import E2EEncryptionPlugin from "./plugins/E2EEncryptionPlugin";
import UserPlugin from "./plugins/UserPlugin";
import {LoginModule} from "./modules/LoginModule";
import {GroupModule} from "./modules/GroupModule";
import JosePlugin from "./plugins/JosePlugin";


class App {

    readonly fastify: FastifyInstance<Server, IncomingMessage, ServerResponse, FastifyLoggerInstance>;

    constructor() {
        this.fastify = Fastify({trustProxy: true});


    }
    build(): FastifyInstance<Server, IncomingMessage, ServerResponse, FastifyLoggerInstance> {
        this.fastify.register(EncryptionPlugin);
        this.fastify.register(StoragePlugin);
        this.fastify.register(SessionPlugin);
        this.fastify.register(HashPlugin);
        this.fastify.register(JosePlugin);
        this.fastify.register(E2EEncryptionPlugin);
        this.fastify.register(UserPlugin);
        this.getModules().forEach(module => {
            module.initialize();
        });
        return this.fastify;
    }
    getModules():Array<AppModule> {
    return [
            new PingModule(this.fastify),
            new TokenModule(this.fastify),
            new RegistrationModule(this.fastify),
            new LoginModule(this.fastify),
            new GroupModule(this.fastify),
     //       new PasswordModule(this.fastify)
        ];
    }
    async start() {
        // Google Cloud Run will set this environment variable for you, so
        // you can also use it to detect if you are running in Cloud Run
        const IS_GOOGLE_CLOUD_RUN = process.env.K_SERVICE !== undefined

        // You must listen on the port Cloud Run provides
        const port = process.env.PORT || 3000

        // You must listen on all IPV4 addresses in Cloud Run
        const address: string | undefined = IS_GOOGLE_CLOUD_RUN ? "0.0.0.0" : undefined

        try {
            const server = this.build()

            const listener = await server.listen(port, address)
            console.log(`Listening on ${listener}`)
        } catch (err) {
            console.error(err)
            process.exit(1)
        }
    }
}


module.exports = () => {
    const app: App = new App();
    return app.build();
}

if (require.main === module) {
    const app: App = new App();
    app.start()
}