export interface SessionData {
    privateKey: string,
    communicationKey: null|string,
    userId: string
}