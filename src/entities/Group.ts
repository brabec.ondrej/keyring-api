import {EncryptedPrivateKey} from "./User";

export interface Group {
    name: string
    publicKey: string
}