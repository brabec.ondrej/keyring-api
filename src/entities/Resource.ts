export interface Resource {
    username: string
    password: string
    groupId: string
}