export interface Key {
    publicKey: string
    privateKey: string
}
export interface GeneralKey extends Key {
    jwtKey: string
}