import {EncryptedResult, Key} from "../utils/SymetricEncryptionUtil";

export interface User {
    email: string
    password: string
    publicKey: string
    privateKey: EncryptedPrivateKey
    groups: GroupAccess[]
}

export interface UserAuthenticated {
    id: string
    publicKey: string
    privateKey: string
}
export interface EncryptedPrivateKey {
    salt: Buffer | null,
    encrypted: string
    iv: string
    authTag: string
}

interface GroupAccess {
    groupId: string,
    privateKey: string
}