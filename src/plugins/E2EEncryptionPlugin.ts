import {FastifyRequest} from "fastify/types/request";
import {FastifyReply} from "fastify/types/reply";
import {EncryptedBody} from "./EncryptionPlugin";
import {FastifyPluginAsync} from "fastify";
import fp from "fastify-plugin";
import {Key} from "../entities/Key";

declare module 'fastify' {
    interface FastifyRequest {
        e2eEncryption: boolean
        publicKey: null|string
    }

    interface FastifyReply {
        e2eEncryption: boolean
    }
}

export class E2EEncryptionUtil {
    async decryptPayload(request: FastifyRequest, reply: FastifyReply): Promise<void> {
        request.publicKey = null;
        if (request.e2eEncryption && ["POST","PUT"].includes(request.method)) {
            let storage = await request.firestore.collection("tokens").doc("general").get();

            if (!storage.exists) {
                throw Error("Unable to process request");
            }

            let body = request.body as EncryptedBody;
            let keys = storage.data() as Key;
            let decryptedBody :E2EEncryptionRequestBody;
            decryptedBody = (await request.decrypt(body, keys.privateKey)) as E2EEncryptionRequestBody;
            request.body = decryptedBody;

            request.publicKey = decryptedBody.publicKey;
        }
    }


    async encryptPayload(request: FastifyRequest<{ Body: E2EEncryptionRequestBody }>, reply: FastifyReply, payload: object): Promise<object|string> {
        if (reply.e2eEncryption && request.publicKey !== null) {
            let publicKey :string;
            publicKey = request.publicKey;

            return JSON.stringify(await reply.encrypt(payload, publicKey));
        }
        return payload;
    }
}

interface E2EEncryptionRequestBody {
    publicKey: string;
}

const e2eEncryptionPlugin: FastifyPluginAsync = async (fastify, options) => {
    const e2eEncryptionUtil: E2EEncryptionUtil = new E2EEncryptionUtil()
    fastify.addHook("onRequest", (request, reply, done) => {
        request.e2eEncryption = true;
        reply.e2eEncryption = true;
        done();
    });

    fastify.addHook("preValidation", e2eEncryptionUtil.decryptPayload)
    fastify.addHook("onSend", e2eEncryptionUtil.encryptPayload)
}

export default fp(e2eEncryptionPlugin);
