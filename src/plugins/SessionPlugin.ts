import {CollectionReference, DocumentData} from "@google-cloud/firestore";
import {SessionData} from "../entities/Session";
import {getFirestore} from "./StoragePlugin";
import {FastifyPluginAsync} from "fastify";
import fp from "fastify-plugin";
import {EncryptedBody} from "./EncryptionPlugin";

export interface GetSessionRequest {
    (token: string): Promise<SessionData>
}

export interface SetSessionRequest {
    (data: SessionData): Promise<void>
}

declare module 'fastify' {
    interface FastifyRequest {
        getSession: GetSessionRequest
        setSession: SetSessionRequest
    }
}

export class SessionStorage {

    async getSession(token: string): Promise<SessionData> {
        let doc = await getFirestore().collection("sessions").doc(token).get();
        if (!doc.exists) {
            throw Error("Session token data does not exists");
        }
        return doc.data() as SessionData;
    }

    async setSession(data: SessionData): Promise<string> {
        let doc =  await getFirestore().collection("sessions").add(data);
       return doc.id;
    }
}

const sessionStoragePlugin: FastifyPluginAsync = async (fastify, options) => {
    let sessionStorage = new SessionStorage();
    fastify.decorateRequest("getSession", sessionStorage.getSession);
    fastify.decorateRequest("setSession", sessionStorage.setSession);
}

export default fp(sessionStoragePlugin)
