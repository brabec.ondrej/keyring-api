import {FastifyPluginAsync} from 'fastify'
import fp from 'fastify-plugin'
import {Firestore} from "@google-cloud/firestore";
import {SessionData} from "../entities/Session";
import {GetSessionRequest} from "./SessionPlugin";
import crypto from "crypto";

export interface HashRequest {
    (textToHash: string): string
}

export interface HashValidateRequest {
    (textToValidate: string, hash: string): boolean
}

declare module 'fastify' {
    interface FastifyRequest {
        hashString: HashRequest
        hashValidate: HashValidateRequest
    }
}

export const hashString: HashRequest = (textToHash: string): string => {
    //creating hash object
    let hash = crypto.createHash('sha384');
    //passing the data to be hashed
    let data = hash.update(textToHash, 'utf-8');
    //Creating the hash in the required format
    return data.digest('base64');
}

export const validateString: HashValidateRequest = (textToValidate: string, hash: string): boolean => {
    return hashString(textToValidate) === hash;
}

const hashStringPlugin: FastifyPluginAsync = async (fastify, options) => {
    fastify.decorateRequest("hashString", hashString);
    fastify.decorateRequest("hashValidate", validateString);
}

export default fp(hashStringPlugin);