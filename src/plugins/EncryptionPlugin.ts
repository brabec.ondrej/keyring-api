import {FastifyPluginAsync} from 'fastify'
import fp from 'fastify-plugin'
import {EncryptionUtil} from "../utils/EncryptionUtil";
import {Base64ArrayBufferConvertor} from "../utils/Base64ArrayBufferConvertor";
import {SYMETRIC_ENCRYPTION_MODE, SymetricEncryptionUtil} from "../utils/SymetricEncryptionUtil";

export interface DecryptRequest {
    (requestBody: EncryptedBody, privateKey: string): Promise<object>
}

export interface EncryptRequest {
    (responseBody: object, publicKey: string): Promise<EncryptedBody>
}

declare module 'fastify' {
    interface FastifyRequest {
        decrypt: DecryptRequest
    }

    interface FastifyReply {
        encrypt: EncryptRequest
    }
}

export class EncryptionPlugin {
    static async encrypt(responseBody: object|string, publicKey: string): Promise<EncryptedBody> {
        const SymmetricEncryptionUtil = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS);

        if(typeof responseBody === 'object' && responseBody !== null) {
            responseBody =JSON.stringify(responseBody);
        }

        const bodyString = Buffer.from(responseBody, "utf8").toString("base64");
        let {key, encrypted, iv, authTag} = SymmetricEncryptionUtil.encrypt(bodyString);

        let stringKey = Buffer.from(JSON.stringify(key), "utf8").toString("base64");
        let encryptedKey = Base64ArrayBufferConvertor.encode(await EncryptionUtil.encrypt(Base64ArrayBufferConvertor.decode(stringKey), Base64ArrayBufferConvertor.decode(publicKey)));

        return {
            key: encryptedKey,
            payload: encrypted,
            iv,
            authTag
        };

    }

    static async decrypt(requestBody: EncryptedBody, privateKey: string): Promise<object|Array<object>> {
        const SymmetricEncryptionUtil = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.RANDOM_PASS);

        //Asymmetric decryption od key
        let keyRaw = await EncryptionUtil.decrypt(Base64ArrayBufferConvertor.decode(requestBody.key), Base64ArrayBufferConvertor.decode(privateKey));
        // Get key to json from Raw format
        let explodedKey = JSON.parse(Buffer.from(Base64ArrayBufferConvertor.encode(keyRaw), "base64").toString("utf8"));
        explodedKey.secret = Buffer.from(explodedKey.secret)

        SymmetricEncryptionUtil.setKey(explodedKey);

        // Decrypt payload by symmetricUtil
        let rawPayload = SymmetricEncryptionUtil.decrypt(requestBody.payload, requestBody.iv, requestBody.authTag);
        // Return formatted object
        return JSON.parse(Buffer.from(rawPayload, "base64").toString("utf8"));
    }
}

// define plugin using promises
const encryption: FastifyPluginAsync = async (fastify, options) => {
    fastify.decorateRequest('decrypt', EncryptionPlugin.decrypt);
    fastify.decorateReply('encrypt', EncryptionPlugin.encrypt);
}


export interface EncryptedBody {
    key: string;
    payload: string
    iv: string
    authTag: string
}

export default fp(encryption)