import {User, UserAuthenticated} from "../entities/User";
import {FastifyPluginAsync} from "fastify";
import fp from "fastify-plugin";
import {JWE} from "node-jose";
import {EncryptionUtil} from "../utils/EncryptionUtil";
import {SessionData} from "../entities/Session";
import {Base64ArrayBufferConvertor} from "../utils/Base64ArrayBufferConvertor";


declare module 'fastify' {
    interface FastifyRequest {
        user: null | UserAuthenticated
    }
}


const userPlugin: FastifyPluginAsync = async (fastify, options) => {
    fastify.addHook('preHandler', async (request, reply) => {
        if (request.headers.authorization !== undefined) {
            let token = request.headers.authorization.replace("Bearer ", "");
            try {
                let decrypted = await JWE.createDecrypt(request.keystore)
                    .decrypt(token);
                let data = JSON.parse(decrypted.payload.toString("utf-8"));
                let session = await request.getSession(data.sessionKey);

                let userDoc = await request.firestore.collection("users").doc(session.userId).get();
                if (userDoc.exists) {

                    let tokens = await request.firestore.collection("tokens").doc(data.keyId).get();

                    if(!tokens.exists) throw new Error("Wrong token")

                    let privateKey = Base64ArrayBufferConvertor.encode(await EncryptionUtil.decrypt(Base64ArrayBufferConvertor.decode(session.privateKey),Base64ArrayBufferConvertor.decode(tokens.data()?.privateKey)));
                    let userData = userDoc.data() as User;

                    request.user = {
                        id: userDoc.id,
                        publicKey: userData.publicKey,
                        privateKey
                    };

                    if(request.publicKey === null) {
                        request.publicKey = session.communicationKey;
                    }
                } else {
                    request.user = null
                }
            } catch (e) {
                reply.code(403).send({ message:"Unable to process authentication!"})

            }
        } else {
            request.user = null;
        }
    })
}

export default fp(userPlugin);