import {JWK} from "node-jose";
import KeyStore = JWK.KeyStore;
import {Firestore} from "@google-cloud/firestore";
import {FastifyPluginAsync} from "fastify";
import fp from "fastify-plugin";
import {SessionData} from "../entities/Session";

export interface GetKeyRequest {
    (): Promise<string>
}

declare module 'fastify' {
    interface FastifyRequest {
        keystore: KeyStore
        getJWKKey:GetKeyRequest
    }
}

const firestore: FastifyPluginAsync = async (fastify, options) => {
    fastify.addHook("onRequest", async (request, reply) => {
        let doc = await request.firestore.collection("tokens").doc("jwk").get();
        if (!doc.exists) {
            request.keystore = JWK.createKeyStore();
        } else {
            request.keystore = await JWK.asKeyStore(doc.data() as object);
        }
    });
    fastify.addHook("onResponse", async (request, reply) => {

        let keys = request.keystore.toJSON(true);
        let doc = request.firestore.collection("tokens").doc("jwk")

        await doc.set(keys);
    });
}

export default fp(firestore)