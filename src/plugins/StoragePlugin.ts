import {FastifyPluginAsync} from 'fastify'
import fp from 'fastify-plugin'
import {Firestore} from "@google-cloud/firestore";

declare module 'fastify' {
    interface FastifyRequest {
        firestore: Firestore
    }
}
export const getFirestore = () => {
    return new Firestore({
        projectId: 'keyring-app',
        keyFilename: __dirname + '/../../keys/storage.json'
    });
}

const firestore: FastifyPluginAsync = async (fastify, options) => {
    fastify.addHook("onRequest", (request, reply,done) => {
        request.firestore = getFirestore();
        done();
    });
}

export default fp(firestore)