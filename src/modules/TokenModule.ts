import {FastifyInstance, FastifyReply, FastifyRequest, RouteShorthandOptions} from "fastify";

import {AppModule} from "./AppModule";
import {EncryptionUtil} from "../utils/EncryptionUtil";
import {Base64ArrayBufferConvertor} from "../utils/Base64ArrayBufferConvertor";
import {getFirestore} from "../plugins/StoragePlugin";
import {Key} from "../entities/Key";
import {randomBytes} from "crypto";

export class TokenModule implements AppModule {
    private _server: FastifyInstance;
    constructor(server: FastifyInstance) {
        this._server = server;
    }
    initialize(): void {
        this._server.addHook('onReady', TokenModule.initMainToken)
        this._server.get("/token", this.getConfig(),this.actionToken);
    }

    getConfig(): RouteShorthandOptions {
        return {
            schema: {
                response: {
                    200: {
                        type: 'object',
                        properties: {
                            publicKey: {type: 'string'}
                        }
                    }
                }

            },
            onRequest: (request, reply, done) => {
                request.e2eEncryption = false;
              reply.e2eEncryption = false;
              done();
            }
        }
    }

    async actionToken(req: FastifyRequest, reply: FastifyReply) {

        let mainTokens = await TokenModule.getMainToken(req);
        if (mainTokens === null) {
            await TokenModule.initMainToken();
            mainTokens = await TokenModule.getMainToken(req) as Key;
        }

        reply.send({
            publicKey: mainTokens.publicKey
        });
    }

    private static async initMainToken() {
        let db = getFirestore();
        let doc = db.collection("tokens").doc("general");
        let keypair = await EncryptionUtil.generateKeys();

        await doc.set(
            {
                "publicKey": Base64ArrayBufferConvertor.encode(await keypair.publicKey),
                "privateKey": Base64ArrayBufferConvertor.encode(await keypair.privateKey),
                "jwtKey": Base64ArrayBufferConvertor.encode(randomBytes(32))
            }
        );
    }

    static async getMainToken(req: FastifyRequest): Promise<Key | null> {
        let db = req.firestore;
        let keysStorage = await db.collection('tokens').doc("general").get();
        if (!keysStorage.exists) {
            return null;
        }

        return keysStorage.data() as Key;
    }
}