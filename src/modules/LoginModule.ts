import {AppModule} from "./AppModule";
import {FastifyInstance, RouteShorthandOptions} from "fastify";
import {FastifyRequest} from "fastify/types/request";
import {FastifyReply} from "fastify/types/reply";
import LoginRequestSchema from "../schemas/LoginRequestSchema.json";
import LoginResponseSchema from "../schemas/LoginResponseSchema.json";
import {LoginRequestInterface} from "../types/LoginRequestSchema";
import {LoginResponseInterface} from "../types/LoginResponseSchema";
import {User} from "../entities/User";
import {SYMETRIC_ENCRYPTION_MODE, SymetricEncryptionUtil} from "../utils/SymetricEncryptionUtil";
import {Base64ArrayBufferConvertor} from "../utils/Base64ArrayBufferConvertor";
import {SessionData} from "../entities/Session";
import {Key} from "../entities/Key";
import {EncryptionUtil} from "../utils/EncryptionUtil";
import {JWE} from "node-jose";


export class LoginModule implements AppModule {
    private readonly _server: FastifyInstance;

    constructor(server: FastifyInstance) {
        this._server = server;
    }

    initialize(): void {
        this._server.post<{ Body: LoginRequestInterface }>("/login", this.getConfig() as RouteShorthandOptions, this.actionLogin)
    }

    getConfig() {
        return {
            schema: {
                body: LoginRequestSchema,
                response: {
                    200: LoginResponseSchema
                }
            }
        }
    }

    async actionLogin(request: FastifyRequest<{ Body: LoginRequestInterface }>, reply: FastifyReply) {
        const collection = await request.firestore.collection("users").where("email", "==", request.body.email).get();

        // User existence check
        if (collection.empty) {
            let error = new Error("User doesn't exists!");
            reply.code(422).send(error);
        }
        const userDoc = collection.docs[0];

        const userData = userDoc.data() as User;
        // Password check
        if (!request.hashValidate(request.body.password, userData.password)) {
            let error = new Error("Wrong password!");
            reply.code(403).send(error);
        }

        let symmetricEncryption = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.USER_PASS);
        symmetricEncryption.setPassword(request.body.password, userData.privateKey.salt);
        // Unlock the user private Code

        let decryptedPrivate = symmetricEncryption.decrypt(userData.privateKey.encrypted, userData.privateKey.iv, userData.privateKey.authTag);

        // Create Session for user
        // Generate keys for db lock
        let ntruKeys = await EncryptionUtil.generateKeys();

        let keys: Key = {
            privateKey: Base64ArrayBufferConvertor.encode(ntruKeys.privateKey),
            publicKey: Base64ArrayBufferConvertor.encode(ntruKeys.publicKey)
        }


        // Save keys in unconnected db

        await request.firestore.collection("tokens").doc().set(keys);

        const keyDoc = (await request.firestore.collection("tokens").where("publicKey", "==", keys.publicKey).get()).docs[0];
        // Create session with encrypted private key
        let session: SessionData = {
            privateKey: Base64ArrayBufferConvertor.encode(await EncryptionUtil.encrypt(Base64ArrayBufferConvertor.decode(decryptedPrivate), ntruKeys.publicKey)),
            communicationKey: request.publicKey,
            userId: userDoc.id
        }


        let sessionKey = request.setSession(session);

        // Create JWT token send with keyDoc id for session unlock
        let jwk = await request.keystore.generate("oct", 256, {alg: "A256GCM", use: "enc"})

        let data = Buffer.from(JSON.stringify({
            keyId: keyDoc.id,
            sessionKey: await sessionKey
        }), "utf-8");

        let token = await JWE.createEncrypt({format: 'compact', zip: true}, jwk)
            .update(data)
            .final();


        // Send JWT to user
        reply.send({token});
    }
}