import {AppModule} from "./AppModule";
import {FastifyInstance, RouteShorthandOptions} from "fastify";
import {FastifyRequest} from "fastify/types/request";
import {FastifyReply} from "fastify/types/reply";


import RegistrationRequestSchema from '../schemas/RegistrationRequestSchema.json';
import {RegistrationRequestInterface} from "../types/RegistrationRequestSchema";
import StatusResponseSchema from '../schemas/StatusResponseSchema.json';
import {User} from "../entities/User";
import {EncryptionUtil} from "../utils/EncryptionUtil";
import {Base64ArrayBufferConvertor} from "../utils/Base64ArrayBufferConvertor";
import {SYMETRIC_ENCRYPTION_MODE, SymetricEncryptionUtil} from "../utils/SymetricEncryptionUtil";
import {Group} from "../entities/Group";


export class RegistrationModule implements AppModule {

    private readonly _server: FastifyInstance;

    constructor(server: FastifyInstance) {
        this._server = server;
    }

    initialize(): void {
        this._server.post<{
            Body: RegistrationRequestInterface
        }>("/register", this.getConfig() as RouteShorthandOptions, this.actionRegister);
    }

    getConfig() {
        return {
            schema: {
                body: RegistrationRequestSchema,
                response: {
                    200: StatusResponseSchema
                }

            },
        }
    }

    async actionRegister(request: FastifyRequest<{ Body: RegistrationRequestInterface }>, reply: FastifyReply) {
        const users = request.firestore.collection("users");
        // Already registered user
        const exists = await users.where("email", "==", request.body.email).get();
        if (!exists.empty) {
            let error = new Error("User already exists!");
            reply.code(409).send(error);
        } else {
            // Register user
            let keys = await EncryptionUtil.generateKeys();
            let symmetricEncryption = new SymetricEncryptionUtil(SYMETRIC_ENCRYPTION_MODE.USER_PASS);
            symmetricEncryption.setPassword(request.body.password);

            const groupKeys = await EncryptionUtil.generateKeys();
            let group: Group = {
                name: "Personal",
                publicKey: Base64ArrayBufferConvertor.encode(groupKeys.publicKey)
            };

            let savedGroup = await request.firestore.collection("groups").add(group);
            let privateKey = Base64ArrayBufferConvertor.encode(await EncryptionUtil.encrypt(groupKeys.privateKey,keys.publicKey));

            let encryptedPrivate = symmetricEncryption.encrypt(Base64ArrayBufferConvertor.encode(keys.privateKey));
            let user: User = {
                email: request.body.email,
                publicKey: Base64ArrayBufferConvertor.encode(keys.publicKey),
                password: request.hashString(request.body.password),
                privateKey: {
                    encrypted: encryptedPrivate.encrypted,
                    iv: encryptedPrivate.iv,
                    authTag: encryptedPrivate.authTag,
                    salt: encryptedPrivate.key.salt
                },
                groups: [
                    {
                        groupId: savedGroup.id,
                        privateKey:privateKey
                    }
                ]
            };

            await users.doc().set(user);

            // Return status of registration
            reply.send({
                "status": "OK",
                "message": "Everything is ok"
            })
        }
    }

}
