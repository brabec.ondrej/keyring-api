import { FastifyInstance, FastifyLoggerInstance, FastifyReply, FastifyRequest, RouteShorthandOptions } from "fastify";
import { AppModule } from "./AppModule";

export class PingModule implements AppModule {
    private _server: FastifyInstance;
    constructor(server:FastifyInstance) {
        this._server = server;
    }
    initialize(): void {
        const opts: RouteShorthandOptions = {
            schema: {
                response: {
                    200: {
                        type: 'object',
                        properties: {
                            pong: {
                                type: 'string'
                            }
                        }
                    }
                }
            },
            onRequest: (request, reply) => {
                request.e2eEncryption = false;
                reply.e2eEncryption = false;
            }
        }

        this._server.get('/ping', opts, this.actionPing)
    }

    actionPing(request: FastifyRequest, reply: FastifyReply) {

        reply.code(200).send({ pong: 'it worked!' })
    }

}