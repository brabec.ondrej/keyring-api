import {AppModule} from "./AppModule";
import {FastifyInstance, RouteShorthandOptions} from "fastify";
import {AuthenticationHeadersInterface} from "../types/AuthentificationHeaderSchema";
import AuthenticationHeadersSchema from "../schemas/AuthentificationHeaderSchema.json";
import GroupResponseSchema from "../schemas/GroupsResponseSchema.json";
import GroupDetailParamsSchema from "../schemas/GroupDetailParamsSchema.json";
import GroupDetailResponseSchema from "../schemas/GroupDetailResponseSchema.json";
import GroupAddBodySchema from "../schemas/GroupAddBodySchema.json";
import StatusResponseSchema from "../schemas/StatusResponseSchema.json";
import {FastifyRequest} from "fastify/types/request";
import {FastifyReply} from "fastify/types/reply";
import {User} from "../entities/User";
import {GroupDetailParamsInterface} from "../types/GroupDetailParamsSchema";
import {ArrayUtil} from "../utils/Array";
import {Group} from "../entities/Group";
import {GroupDetailResponseInterface} from "../types/GroupDetailResponseSchema";
import {GroupAddBodyInterface} from "../types/GroupAddBodySchema";
import {EncryptionUtil} from "../utils/EncryptionUtil";
import {Base64ArrayBufferConvertor} from "../utils/Base64ArrayBufferConvertor";

export class GroupModule implements AppModule {

    private readonly _server: FastifyInstance;

    constructor(server: FastifyInstance) {
        this._server = server;
    }

    initialize(): void {
        this._server.get<{ Headers: AuthenticationHeadersInterface }>("/groups", this.getListConfig() as RouteShorthandOptions, this.actionGroupList)
        this._server.get<{ Headers: AuthenticationHeadersInterface, Params: GroupDetailParamsInterface }>("/group/:groupId", this.getDetailConfig() as RouteShorthandOptions, this.actionGroupDetail)
        this._server.post<{ Headers: AuthenticationHeadersInterface, Body: GroupAddBodyInterface }>("/group", this.getAddConfig() as RouteShorthandOptions, this.actionGroupAdd)
        this._server.delete<{ Headers: AuthenticationHeadersInterface, Params: GroupDetailParamsInterface }>("/group/:groupId", this.getRemoveConfig() as RouteShorthandOptions, this.actionGroupRemove)
    }

    private getListConfig(): RouteShorthandOptions {
        return {
            schema: {
                headers: AuthenticationHeadersSchema,
                response: {
                    200: GroupResponseSchema
                }
            },
        }
    }

    private getDetailConfig(): RouteShorthandOptions {
        return {
            schema: {
                headers: AuthenticationHeadersSchema,
                params: GroupDetailParamsSchema,
                response: {
                    200: GroupDetailResponseSchema
                }
            },
        }
    }

    private getAddConfig(): RouteShorthandOptions {
        return {
            schema: {
                headers: AuthenticationHeadersSchema,
                body: GroupAddBodySchema,
                response: {
                    200: StatusResponseSchema
                }
            },
        }
    }

    private getRemoveConfig(): RouteShorthandOptions {
        return {
            schema: {
                headers: AuthenticationHeadersSchema,
                params: GroupDetailParamsSchema,
                response: {
                    204: StatusResponseSchema
                }
            },
        }
    }

    private async actionGroupList(request: FastifyRequest<{ Headers: AuthenticationHeadersInterface }>, reply: FastifyReply) {

        if (request.user === null) throw new Error("Unauthenticated");

        let user = await request.firestore.collection("users").doc(request.user.id).get();

        if (!user.exists) throw new Error("Non-existent");

        let userData = user.data() as User;
        let groups = [];
        for (const userDataGroupId in userData.groups) {
            let group = await request.firestore.collection("groups").doc(userData.groups[userDataGroupId].groupId).get();

            if (group.exists) {
                let groupData = group.data() as Group;

                groups.push({
                    id: group.id,
                    name: groupData.name,
                    publicKey: groupData.publicKey,

                })
            }
        }
        reply.send(groups);
    }

    private async actionGroupDetail(request: FastifyRequest<{ Headers: AuthenticationHeadersInterface, Params: GroupDetailParamsInterface }>, reply: FastifyReply) {
        if (request.user === null) throw new Error("Unauthenticated");

        let user = await request.firestore.collection("users").doc(request.user.id).get();

        if (!user.exists) throw new Error("Non-existent");

        let userData = user.data() as User;
        let group = await request.firestore.collection("groups").doc(request.params.groupId).get();

        if (!group.exists || !ArrayUtil.search(userData.groups, request.params.groupId, "groupId")) throw new Error("Non-existent");

        let resources = await request.firestore.collection("resources").where("groupId", "==", group.id).get()
        let groupData = group.data() as Group;

        let data: GroupDetailResponseInterface;
        data = {
            "name": groupData.name,
            "publicKey": groupData.publicKey,
            "resources": []
        };
        resources.forEach((result) => {
            if (!result.exists) throw new Error("Non-existent");

            data.resources.push(result.data());
        });
        reply.send(data)
    }

    private async actionGroupAdd(request: FastifyRequest<{ Headers: AuthenticationHeadersInterface, Body: GroupAddBodyInterface }>, reply: FastifyReply) {
        if (request.user === null) throw new Error("Unauthenticated");

        let user = await request.firestore.collection("users").doc(request.user.id).get();

        if (!user.exists) throw new Error("Non-existent");

        let userData = user.data() as User;

        let keys = await EncryptionUtil.generateKeys();
        let newGroup: Group = {
            name: request.body.name,
            publicKey: Base64ArrayBufferConvertor.encode(keys.publicKey),
        }
        let savedGroup = await request.firestore.collection("groups").add(newGroup);

        let privateKey = Base64ArrayBufferConvertor.encode(await EncryptionUtil.encrypt(keys.privateKey, Base64ArrayBufferConvertor.decode(userData.publicKey)));

        userData.groups.push({
            groupId: savedGroup.id,
            privateKey: privateKey
        })

        await request.firestore.collection("users").doc(request.user.id).set(userData);
        reply.statusCode = 201;
        reply.send({
            "status": "OK",
            "message": "Group was created"
        })
    }

    private async actionGroupRemove(request: FastifyRequest<{ Headers: AuthenticationHeadersInterface, Params: GroupDetailParamsInterface }>, reply: FastifyReply) {
        if (request.user === null) throw new Error("Unauthenticated");

        let user = await request.firestore.collection("users").doc(request.user.id).get();

        if (!user.exists) throw new Error("Non-existent");

        let userData = user.data() as User;

        let group = await request.firestore.collection("groups").doc(request.params.groupId).get();

        if (!group.exists || !ArrayUtil.search(userData.groups, request.params.groupId, "groupId")) throw new Error("Non-existent or un-accessible");

        let resources = await request.firestore.collection("resources").where("groupId", "==", group.id).get()
        const batch = request.firestore.batch();
        resources.docs.forEach((doc) => {
            if (doc.exists) {
                batch.delete(doc.ref);
            }
        });
        batch.delete(group.ref);
        await batch.commit();

        reply.statusCode = 204;
        reply.send({
            "status": "OK",
            "message": "Group was removed"
        });

    }
}