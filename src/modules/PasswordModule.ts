import {AppModule} from "./AppModule";
import {FastifyInstance} from "fastify";

export class PasswordModule implements AppModule{
    private readonly _server: FastifyInstance;

    constructor(server: FastifyInstance) {
        this._server = server;
    }

    initialize(): void {
    }

}