import { FastifyInstance } from "fastify";

export interface AppModule {
    initialize():void
}